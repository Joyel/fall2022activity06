package geometry;

public class LotsOfShapes {
    public static void main(String[] args){
        Shape[] fiveShapes = new Shape[5];
        fiveShapes[0] = new Circle(5);
        fiveShapes[1] = new Circle(4.08);
        fiveShapes[2] = new Rectangle(8, 16);
        fiveShapes[3] = new Rectangle(12.7, 22.1);
        fiveShapes[4] = new Square(4.0);

        for (Shape s : fiveShapes){
            System.out.println("Area: " + s.getArea() + " | Perimeter: " + s.getPerimeter());
        }
    }
}
