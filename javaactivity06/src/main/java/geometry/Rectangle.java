package geometry;

public class Rectangle implements Shape {
    private double length;
    private double width;

    public Rectangle(double newLength, double newWidth){
        this.length = newLength;
        this.width = newWidth;
    }

    public double getLength(){
        return this.length;
    }

    public double getWidth(){
        return this.width;
    }

    @Override
    public double getArea(){
        return this.length * this.width;
    }

    @Override
    public double getPerimeter(){
        return 2 * (this.length + this.width);
    }
}
